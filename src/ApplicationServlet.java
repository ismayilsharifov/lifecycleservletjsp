import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ApplicationServlet",urlPatterns = "/app")
public class ApplicationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getParameter("action")!=null){
            String action = request.getParameter("action");
            System.out.println("action = " + action);
            if(action.equals("increase")){
                if(getServletContext().getAttribute("counter")==null){
                    System.out.println("counter not exist");
                    getServletContext().setAttribute("counter",1);
                }else{
                    int counter = (int) getServletContext().getAttribute("counter");
                    getServletContext().setAttribute("counter",counter+1);
                    System.out.println("Counter = " + ( counter+1));
                }
            }
            if(action.equals("decrease")){
                if(getServletContext().getAttribute("counter")==null){
                    System.out.println("counter not exist");
                    getServletContext().setAttribute("counter",1);
                }else{
                    int counter = (int) getServletContext().getAttribute("counter");
                    getServletContext().setAttribute("counter",counter-1);
                    System.out.println("Counter = " + (counter-1));
                }
            }
        }
        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        if(getServletContext().getAttribute("counter")!=null){
            int counter = (int)getServletContext().getAttribute("counter");
            out.println("counter = " + counter);
        }else{
            out.println("counter not exist");
        }
    }
}
