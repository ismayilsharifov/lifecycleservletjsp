import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.EmailValidator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormValidation {

    public static Map<String, List<String>> validate(FormData form){
        Map<String, List<String>> errorMap = new HashMap<>();
        List<String>nameErrors = new ArrayList<>();
        List<String>surnameErrors = new ArrayList<>();
        List<String>emailErrors = new ArrayList<>();
        List<String>salaryErrors = new ArrayList<>();
        boolean isBlankOrNull = false;
        if(GenericValidator.isBlankOrNull(form.getName())){
            nameErrors.add("Ad bosh ola bilmez");
            isBlankOrNull=true;
        }

        if(!isBlankOrNull && !GenericValidator.isInRange(form.getName().length(),3,50)){
            nameErrors.add("Ad minimum 3 maksimum 50 herf ola biler");
        }
        if(!nameErrors.isEmpty()){
            errorMap.put("name",nameErrors);
        }

        isBlankOrNull=false;
        if(GenericValidator.isBlankOrNull(form.getSurname())){
            surnameErrors.add("Soyad bosh ola bilmez");
            isBlankOrNull=true;
        }
        if(!isBlankOrNull && !GenericValidator.isInRange(form.getSurname().length(),3,50)){
            surnameErrors.add("Soyad minimum 3 maksimum 50 herf ola biler");
        }
        if(!surnameErrors.isEmpty()){
            errorMap.put("surname",surnameErrors);
        }
        isBlankOrNull=false;
        if(GenericValidator.isBlankOrNull(form.getEmail())){
            emailErrors.add("Email bosh ola bilmez");
            isBlankOrNull=true;
        }
        EmailValidator validator = EmailValidator.getInstance();
        if(!isBlankOrNull && !validator.isValid(form.getEmail())){
            emailErrors.add("Email duzgun tertib olunmayib");
        }
        if(!emailErrors.isEmpty()){
            errorMap.put("email",emailErrors);
        }
        isBlankOrNull = false;
        if(GenericValidator.isBlankOrNull(form.getSalary())){
            isBlankOrNull=true;
        }
        BigDecimal salary = BigDecimal.ONE;
        if(!isBlankOrNull) {
            try {
                salary = new BigDecimal(form.getSalary());
            } catch (Exception e) {
                e.printStackTrace();
                salaryErrors.add("Maash eded olmalidir");
            }
        }else{
            salaryErrors.add("Maash bosh ola bilmez");
        }
        if(salary.compareTo(BigDecimal.ZERO)<=0){
            salaryErrors.add("Maash 0 ve ya menfi ola bilmez");
        }
        if(!salaryErrors.isEmpty()){
            errorMap.put("salary",salaryErrors);
        }
        return errorMap;
    }

}