import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CalculatorServlet",urlPatterns = "/calculator" )
public class CalculatorServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request,response);
    }
    private void process(HttpServletRequest request, HttpServletResponse response)throws ServletException {
        response.setContentType("text/html"); //mime types
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        writer.println("<html>");
        writer.println("<head>");
        writer.println("<title>Calculator</title>");
        writer.println("</head>");
        writer.println("<body>");

        double a = Integer.parseInt(request.getParameter("a"));
        double b = Integer.parseInt(request.getParameter("b"));
        double result = 0;
        String op = request.getParameter("opr");
        if(op.equals("+")){
            result = a + b;
        } if(op.equals("-")){
            result = a - b;
        }if(op.equals("*")){
            result = a * b;
        } if(op.equals("/")){
            if (b == 0) {
                writer.println("Don't divide by zero!");
            } result = a / b;
        } if(op.equals("%")){
            result = a % b;
        } if(op.equals("^")){
            result = Math.pow(a, b);
        }if(op.equals("gcd")){
            result = GCD(a,b);
        }

        writer.println("<h2>Your result: " + result + " </h2>");

        writer.println("</body>");
        writer.println("</html>");
    }
    private static double GCD(double a, double b) {

        if(b!=0){
            return GCD(b,a%b);
        }else return a;
    }

}
