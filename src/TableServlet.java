import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "TableServlet",urlPatterns = "/table")
public class TableServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        int row = 0;
        int column = 0;
        int valid=0;
        try {
             row = Integer.parseInt(request.getParameter("sira"));
             column = Integer.parseInt(request.getParameter("sutun"));
        }catch (NumberFormatException e){
            valid++;
        }
        if(valid==0) {
            if (row > 0 && column > 0) {
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Dynamic Table Generation</title>");
                out.println("<style>");
                out.println("#myHeader{\n" +
                        "background-color: black;\n" +
                        "color: white;\n" +
                        "margin-inline-start: 1px;\n" +
                        "text-align: center;\n" +
                        "font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;\n" +
                        "}");
                out.println("table,td{\n" +
                        "border: 0.5px solid grey;\n" +
                        "background-color: black;\n" +
                        "color: white;\n" +
                        "margin-inline-start: 1px;\n" +
                        "padding: 10px;\n" +
                        "font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;\n" +
                        "border-collapse:collapse;\n" +
                        "}");
                out.println("</style>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h2 id=\"myHeader\">Table generated</h2>");
                out.println("<table>");
                for (int i = 0; i < row; i++) {
                    out.println("<tr>");
                    for (int j = 0; j < column; j++) {
                        out.println("<td>" + (i * column + j + 1));
                    }
                }
                out.println("</table>");
                out.println("</body>");
                out.println("</html>");
            }else{
                out.println("Sira ve sutun musbet ededler olmalidir.");
            }
        }else{
            out.println("Sira ve sutun bosh buraxila bilmez.Musbet ededler olmalidir");
        }
    }


}
