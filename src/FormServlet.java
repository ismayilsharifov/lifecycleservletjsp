import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@WebServlet(name = "FormServlet",urlPatterns = {"/testForm","/form"},
                 loadOnStartup = 1 )
public class FormServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        System.out.println("Form servlet initialized");
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        System.out.println("Form servlet accept post request");

       String name = request.getParameter("ad");
       String surname = request.getParameter("soyad");
       String email = request.getParameter("email");
       String salary = request.getParameter("maas");
        String[] languages =  request.getParameterValues("diller");

        FormData data = new FormData();
        data.setName(name);
        data.setSurname(surname);
        data.setEmail(email);
        data.setSalary(salary);
        data.setLanguages(languages);


        Map<String,List<String>>errorMap = FormValidation.validate(data);
        if(!errorMap.isEmpty()){
            errorMap.forEach((field,errorList)->out.println(field + ": " + errorList));
        }else {
            if(languages!=null) {
                List<String> lang = Arrays.asList(languages);
                out.println("Dil bilikleriniz ");
                lang.forEach(out::println);
            }else{
              out.println("Xarici dil bilikleriniz yox kimi nezere alindi.");
            }
            out.println(name + " " + surname + ", muracietiniz qeyde alindi.Elaqe uchun " + email +
                    " mail-ne  maashinizla " + "(" + salary + " manat)" + " elaqedar cavab " +
                    "gonderilecek.");

        }
    }


    @Override
    public void destroy() {
        System.out.println("Hello servlet finished");
    }


}
