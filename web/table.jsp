
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        #myH1{
            background-color: black;
            color: white;
            margin-inline-start: 1px;
            text-align: center;
            font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        }
        .myH2{
            background-color: black;
            color:white;
           text-align: center;
        }
        .myF{
            background-color: black;
            color:white;
        }

    </style>
    <title >Generator</title>
</head>
<body>
<h1 id="myH1">Dynamic Table Generator</h1>
<h2 class="myH2">Please enter row and column.</h2>
<form method="post" action="table" class="myF">
    <label for="row">  Row </label>
    <input type="number" name="sira" id="row"><br>
    <label for="column">Column </label>
    <input type="number" name="sutun" id="column"><br/>
    <input type="submit" value="Generate">&nbsp;
    <input type="reset" value="Clean">
</form>

</body>
</html>
