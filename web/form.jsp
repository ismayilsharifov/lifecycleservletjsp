<%--
  Created by IntelliJ IDEA.
  User: a1
  Date: 18.06.2020
  Time: 00:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form</title>
    <style>
        .form{
            background-color: black;
            color:white;
            margin: 20px;
            padding: 20px;
            text-align: center;
        }
        #myHeader{
            background-color: black;
            color: white;
            margin-inline-start: 5px;
            text-align: center;
            font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
        }

    </style>

</head>
<body>
<h1 id="myHeader" >Zehmet olmasa formu doldurun</h1>
<form method="post" action="form" class="form">
    <pre>
    <label for="ad" class="">  Ad  : </label>
    <input  type="text" name="ad" id="ad"><br/>
    <label for="soyad">Soyad : </label>
    <input  type="text" name="soyad" id="soyad"><br/>
    <label for="email">Email : </label>
    <input  type="text" name="email" id="email"><br/>
    <label for="maas">Maash : </label>
    <input  type="number"  step="0.01" name="maas" id="maas"><br/>
</pre>
    <label for="diller">Xarici dil bilikleri</label><br/>
    <select name="diller" id="diller" multiple class="form">
        <option value="en">Ingilis</option>
        <option value="ru">Rus</option>
        <option value="tr">Turk</option>
        <option value="de">Alman</option>
        <option value="fr">Fransiz</option>
        <option value="es">Ispan</option>
    </select><br/>
    <input type="submit" value="Gonder">&nbsp;
    <input type="reset" value="Temizle">
</form>
</body>
</html>
